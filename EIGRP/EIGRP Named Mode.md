# EIGRP Named Mode

### In this guide we will cover configuring a simple EIGRP topology using **"named"** mode. 

![](/images/EIGRP%20Named%20mode%20Lab.png)

# Configuration

### First lets see from show commands and our network diagram what networks we need to advertise to our neighbor

    1. R1 - 192.168.10.0/24, 192.168.20.0/24, 24.0.0.0/31
    2. R2 - 10.10.10.0/24, 10.10.20.0/24, 24.0.0.0/31

### Now lets configure EIGRP named mode

# CLI Configuration

`R1(config)#router eigrp TEST` # This command will create a local EIGRP named instance

`R1(config-router)#address-family ipv4 unicast autonomous-system 5` # This command will create an IPv4 EIGRP routing domain. This is how you set the AS number for named EIGRP configuration

`R1(config-router-af)#network [network-address] [wildcard-mask]` # The network command will enable all interfaces that match the network address you input to start sending and receiving EIGRP messages. 

### Full configuration is pictured below

![](/images/EIGRP%20named%20mode%20config%20simple.png)
![](/images/EIGRP%20named%20mode%20config%20simple%20R2.png)

# Verfication

### To verfiy that your EIGRP topology is fully converged we have a LOT of show commands. I will cover the ones I have used the most in day-to-day troubleshooting on an EIGRP domain.

`R1#show ip eigrp neighbors` # This command will verify that you and your neighbor router have exchange EIGRP hello packets and have agreed to become neighbors

![](/images/show%20ip%20eigrp%20neighbors.png)

`R1#show ip eigrp topology` # This command will verify that the EIGRP topology has been built. It will also show the feasible successors and the successor routes. This command will show the reported distance and feasible distance of the routes. 

![](/images/show%20ip%20eigrp%20topology.png)

`R1#show ip eigrp topology all-links` # This command will show you successor routes, feasible successor routes and even the routes that don't meet the feasability condition.

`R1#show ip protocols` # This command will show you all the routing protocols enabled on your routers, and the networks they are advertising.

![](/images/show%20ip%20protocols%20eigrp.png)

`R1show run | section router eigrp` # This command will show us the running configuaration for just our EIGRP configuration.

![](/images/show%20run%20section%20router%20eigrp.png)

`R1#show ip route eigrp` # This command will show us just the EIGRP learned routes on the global routing table.

![](/images/show%20ip%20route%20eigrp.png)

`R1#show ip eigrp interfaces` # This command will show us all the interfaces that are participating in EIGRP

![](/images/show%20ip%20eigrp%20interfaces.png)

# Wireshark

### Now lets look at a packet capture of the EIGRP process. We will take a dive into a hello packet and an update packet. 

### 1. This image shows us the entire process from EIGRP hellos to EIGRP updates and EIGRP acknowledgements. 

![](/images/eigrp%20wireshark%20capture%2001.png)

### 2. This image we will look inside the EIGRP Hello Packet. Inside the EIGRP hello header we can see the opcode (5) which means this is a **Hello** packets, Parameters (K Values which have to match), and the Autonomous System Number which also has to match on both sides.

![](/images/eigrp%20wireshark%20capture%2002.png)

### 3. In this image we will look inside an EIGRP Update Packet. In this packet we see R1 is sending the known networks to R2. R2 will take this update, place these networks into the topology table, run the DUAL algorithm, and place these networks into the global routing table.

![](/images/eigrp%20wireshark%20capture%2003.png)

# Conclusion 

### As we can see in the configurations above configuring EIGRP and getting our routers to dynamically share routes is relatively simple and easy. Now when we get to larger scale networks, and tuning our EIGRP that's a different story. In the upcoming guides we will go over additional EIGRP configuration like summarization, passive-interface, authentication, stub routing, default-information originate, leak-maps, distribute-lists, load-balancing, and offset lists.