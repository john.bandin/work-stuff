# EIGRP Overview

### EIGRP - Enhanced Interior Gateway Routing Protocol. EIGRP is an advanced distance-vector routing protocol created by Cisco. This Routing protocol is classified as an IGP (Interior Gateway Protocol). Up until 2016 EIGRP was a Cisco proprietary routing protocol

### EIGRP uses the algorithm  "DUAL", a Diffusing Update Algorithm as referenced in "Loop-Free Routing Using Diffusing Computations"   (Garcia-Luna-Aceves 1993). -RFC 7868

### EIGRP is is a dynamic routing protocol that creates a loop free path to destination networks based on the best "metric"

![](/images/EIGRP%20Image%201.png)

# EIGRP Neighborship Overiew

### EIGRP Enabled routers will initially send EIGRP **"hello"** packets out their active EIGRP interfaces to form a **"neighborship"** with any EIGRP enabled router with the same parameters

### EIGRP Packets will use something called an **"opcode"** to specifiy which type of EIGRP packets. See image below.

![](/images/EIGRP%20Hello%20Packet%20Breakdown%20(1).png)

### To form **"neighborship"** EIGRP routers will need to have the same values for the following:
    - Autonomous System Number
    - K Values
    - Authentication (if configured)

### Once the neighborship has formed EIGRP will send Hello packets at intervals of 10 seconds (by default). This is called a **"hello-timer"**, EIGRP will also have a **"hold-timer"** which is at default 40 seconds. Once the **"hold-timer"** has reached 40 seconds the EIGRP connection will terminated with a **"goodbye"** message.

### EIGRP will maintain a neighbor table, and from that **"neighbor table"** we will build a topology table

![](/images/EIGRP%20Hello%20Process.png)

# EIGRP Topology Table

### EIGRP will build a topology table from its EIGRP neighbors using the information from it's neighbors "update" packets.

![](/images/R1%20EIGRP%20Topology%20Table.png)

### Once an EIGRP enabled router has received all updates from its neighbors, EIGRP will execute DUAL (diffusion update algorithm), and decide which routes to its neighbors routes are **"best"** and place those routes into the **"global"** routing table.

### EIGRP's DUAL algorithm uses two main values to calcuate the best path to a destination network. Bandwidth and Delay. These values are called "K" values. There is actually a total of 5 K Values used in DUAL. But for brevity, we usually refer to just bandwidth and delay as the two main metrics that get used to determine the best path.

![](/images/eigrp%20dual.png)

### The Global routing is built based on the best routes from the EIGRP topology table. The best routes are called **"successor"** routes. But what about all the other possible routes, that do not have the preferred metric? Those are called **feasable successor"** routes. Feasible successor routes are loop free alternatives routes to the destination. 

![](/images/eigrp%20topology%20show%20command.png)

### EIGRP allows for redundant loop-free paths to destination networks by following what we call the "feasability condition". The feasability condition will take the **"reported distance"** of all routes and ensure that the value for the **"RD"** is not more than the total **"feasible"** distance.

### If a routes **"RD"** is higher than the **"FD"** then the route is not considered a **"feasible successor"**. 

![](/images/Feasibility%20condition.png)

# EIGRP Global Routing Table

### The EIGRP global routing table is built from the successor routes in the topology table. The best metric wins, and gets placed into the routing table.

### EIGRP allows for unequal load-balancing to destination routes. So it is possible to use the feasbile successor to route traffic in the global routing table.

![](/images/EIGRP%20routing%20table.jpg)

