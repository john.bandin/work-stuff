# EIGRP Stub Routing and Leak-Maps

### In this guide we will go over stub routing and configuration. We will also configure a **"leak-map"** to allow stub routers to advertise specific routes to its upstream neighbor.

![](/images/EIGRP%20advanced%20lab%2001.png)

# Stub Overview

### Before configuration lets go over what stub routing is. A stub router or **"stub network"** is a router or routing enabled device that has no downstream routing neighbors to share routes with. A stub router only has one path for destination traffic, and that is to its **ONE** upstream neighbor. This configuration is typical with small offices connected to an HQ (HUB), or in a Hub-and-spoke topology. So why designate a router a **"stub"**. 

# EIGRP Query Messages

### - To understand why we use a stub configuration we must first go over the EIGRP query message. If we take a look at our network diagram above the ISP network has a lot of prefixes being advertised. If one of the prefixes, 3.3.0.0/24 goes down and lets say that network lives on R5, R5 will first send an EIGRP Query message to all its neighbors. The query message will go from R5-->R4, R5-->R3, R5-->R2. Each router will reply with an EIGRP Reply packet saying they do not have an alternative path to 3.3.0.0/24, EXCEPT R2. R2 will send a query downstream, and then R1 will send an EIGRP reply back, then R2 will send an EIGRP reply back to R5 stating there is no alternative path. So no big deal right?

### - In a well tuned and well engineered network we want to try and eliminate any additional stress or burden on the routers physical resources. So, in a large EIGRP domain you can see how all these query messages can be quite intensive. EIGRP will not be fully converged and active until a router recieves a reply to the query. So what can we do to supress the queries? Create a stub network. 

## To make a router a stub it only takes one command. But there are a couple options.

## stub connected -- This configuration will only let the stub router advertise its connected routes to it neighbors. Just the networks physically connected to the router.

## stub connected summary -- This configuration will also advertise a summary route.

## stub connected static -- This allows the redistribution of static routes to be proprogates.

### - Once the stub command has been configured we will see the queries being supressed. We will configure R1 as a stub and then from R2 we will do a debug and check to see if the queries are not getting sent to its downstream stub neighbor R1. We will shutdown loopback address 3.3.3.1 on R5 to cause a topology change so we can see EIGRP queries being sent.

![](/images/EIGRP%20advanced%20lab%2001.png)

![](/images/show%20ip%20eigrp%20neighbors%20detail.png)

### - This command shows us that we are suppressing query messages to our downstream neighbor R1

![](/images/eigrp%20stub%20debug%20commands.png)

### - This debugs will show us where the queries are being sent and the replies.

![shutdown loopback3 on R5 to make a topology change](/images/shutdown%20loopback%203.png)

### - Shutting down this loopback will cause a topology change and query messages to be sent.

![](/images/stub%20debug%20output.png)

### - As we can see here there was no EIGRP Query message sent to the downstream neighbor R1.

![](/images/wireshark%20capture%20stub.png)

### - As you can see from this capture there is truly no queries being sent to our downstream neighbor R1.

# The downside?

### - So what is the downside of configuring a stub network? Once we decide to make R1 a stub router, we no longer can advertise any routes that are not directly connected to us to our neighbor. So lets take a look at our diagram. 

![](/images/EIGRP%20advanced%20lab%2001.png)

### - Our organization has decided to setup another offsite on our campus to service a server farm lets say a mile away. So we add a Cisco 9000series Catalyst Distribution switch. And we want this network to become apart of our routing domain, while also keeping our stub configuration. If we do this, will routers R2 - R5 know about this edition to our routing domain? Let's see. We have configure the DSW and R1 to be neighbors and they are advertising networks between them. If take a look at the DSW's routing table and R5 routing table will we have complete convergence?

![](/images/R5%20routing%20table%20stub.png)

### - As we can see here R5 is not learning about the networks residing on the DSW.

![](/images/DSW%20routing%20table%20stub.png)

### - Our DSW also does not know how to route to any networks besides the connected networks on R1.

### - We can fix this by advertising a default route from R1 ---> DSW. This let all of DSW users and networks use R1 as the default route out. To have full reachability we will need create a "leak-map" on R1 that will advertise the Distro Switches networks to its neighbors, and still supress query messages by remaining a stub router.

# Leak-Map Configuration

### All the configuration we need to do to have complete network convergence for our stub router R1 is done on R1.

### - First we will need to create a default route out to the Distro Switch. 

`R1(config-router-af)#af-interface 0/2` # This command will allow us to configure the eth0/2 interface under our EIGRP named mode

`R1(config-router-af-interface)#summary-address 0.0.0.0 0.0.0.0` # This command well advertise a default route out the interface via EIGRP


### - Lets take a look at this full configuration and the effects as we go. 

![](/images/distro%20switch%20current.png)

![](/images/default%20route%20eigrp.png)

### - Now after this summary-address being sent out to DSW lets recheck the routing table on DSW

![](/images/distro%20switch%20after%20summary%20address.png)

### - Now that the DSW has connectivity to everywhere via the default route lets make sure that the rest of the EIGRP routing domain knows about the routes on DSW.

![](/images/r5%20routing%20table%20stub02.png)

### - As we can see we R5 does not know how to reach the 172.16.10.0/172.16.20.0 networks. So we need to create a prefix-list and route-map to identify those networks and leak them out to the upstream neighbor from R1

`R1(config)#ip prefix-list TO_R2 permit 172.16.10.0/24` # This command will identify the prefix-length to advertise

`R1(config)#ip prefix-list TO_R2 permit 172.16.20.0/24` # This command will identify the prefix-length to advertise

`R1(config)#route-map TO_R2` # This command creates a route-map. Its default is to permit traffic.

`R1(config-route-map)#match ip address prefix-list TO_R2` # This command will identify the prefix-list to be matched.

`R1(config-route-map)#permit 20` # This extra permit command will make sure the rest of the traffic does not get denied.

`R1(config-router-af)#eigrp stub leak-map LEAK_MAP_R2` # This command will advertise routes upstream to R1 that are a part of the downstream routes network, that wouldn't normally be advertised under regular stub routing.

![](/images/stub%20leak%20map%20part%201.png)
![](/images/stub%20leak%20map%20part%202.png)

![](/images/R5%20after%20leak%20map.png)

### - Now we see full convergence on our network and all prefixes and network can now reach each other. We can be sure of this by pinging 3.3.2.1 via the DSW sourced from the 172.16.10.0 network.

![](/images/stub%20ping%20test.png)