# EIGRP Authentication, Passive-interface, and Summarization

### In this guide we will configure authentication keys for our EIGRP neighbors to be used on a yearly basis. We will also configure the "passive-interface" command for any interfaces that do not need to participate in sending EIGRP Hello packets. We will also configure summarization for contiguous networks. 

![](/images/EIGRP%20advanced%20lab.png)

# Configuration

### 1. First we will configure authentication.
### 2. Second we will configure our passive-interfaces
### 3. Lastly we will configure summarization from R1 to R2/R3

### First lets configure our clock and our authentication keys. (typically in a live network we would use NTP for clock synchronization)

`R1#clock set HH:MM:SS DAY MON YEAR` # This command will statically set the clock for the router.

`R1(config)#key chain [NAME]` # This command will create the key chain and bring you into the **"Key Chain"** sub-configuration mode

`R1(config-keychain)#key 1` This command creates an actual key to be used to authenticate. this is creating the PSK, but is not the actuall **"Key"** or **"password"** that will be used.

`R1(config-keychain-key)#key-string [password]` # This command will set the actual password to be used for authentication.

`R1(config-keychain-key)#send-lifetime 00:00:00 1 Mar 2022 00:00:00 28 Feb 2023` # This command will set the length of time the key can be sent.

`R1(config-keychain-key)#accept-lifetime 00:00:00 1 Mar 2022 00:00:00 28 Feb 2023` # This command will set the length of time a key can be accepted

# Full configuration below.

### R1 Configuration
![](/images/set%20clock%20and%20auth%20key%20eigrp.png)

### R2 Configuration
![](/images/set%20clock%20and%20auth%20key%20eigrp%20R2.png)

### R3 Configuration
![](/images/set%20clock%20and%20auth%20key%20eigrp%20R3.png)

# EIGRP Authentication Configuration

### Now lets go into our EIGRP sub-configuration and apply our Key-chain

`R1(config-router-af)#af-interface default` # This command will allow you to configure all interfaces that are participating in EIGRP

`R1(config-router-af-interface)#authentication mode md5` # This command will set the hash method for the Pre-shared key that will be sent in the EIGRP hello packet

`R1(config-router-af-interface)#authentication key-chain {name}` # This command will set the key-chain to be used on the EIGRP enabled interface

# Full configuration below

### This configuration will be applied to all routers

![](/images/eigrp%20authentication%20configuration.png) 

# Verification

`R1#show key chain` # This command will show all the valid key-chain and there lifetimes.

![](/images/show%20key%20chain%20command.png)

### Hello packet with the authentication parameter

![](/images/authentication%20for%20eigrp%20wireshark.png)

# Passive Interface Configuration

### The passive-interface configuration will turn off EIGRP messages being sent out the configured interfaces. We will put every interface into "passive-interface" and then we will go into our individual interfaces that are needed for EIGRP neighbors and turn off the "passive-interface" command.

`R1(config-router-af)#af-interface default` # This command will allow you to configure all interfaces that are participating in EIGRP

`R1(config-router-af-interface)#passive-interface` # This command will turn off EIGRP messages for any inteface

`R1(config-router-af-interface)#no passive-interface` # This command will turn off the passive-interface command and enable EIGRP messages to be sent out the interface

# Full configuration below

![](/images/eigrp%20passive%20interface%20config.png)

# Verification

`R1#show run | section router eigrp` # This command will show us the running configuaration for just our EIGRP configuration.

![](/images/show%20run%20section%20router%20eigrp%20passive.png)

`R1#show ip eigrp interfaces` # This command will show us all the interfaces that are participating in EIGRP

![](/images/show%20ip%20eigrp%20interfaces%20passive.png)

### Wireshark capture of the interface facing the LAN. Notice there are no EIGRP Hello Messages. 

![](/images/wireshark%20for%20passive%20interface.png)

# Summarization

### For this step we will first look at the EIGRP learned routes on R2. 

![](/images/show%20ip%20route%20eigrp%20for%20summarization.png)

### We should see 3 172.16.X.X that can be summarized. Since they are contiguous the routes can be summarized with a /22. We will need to do this configuration for both interfaces from R1-->R2 and R1-->R3.

# Configuration

### First we will enter into our EIGRP named mode configuration.

`R1(config-router-af)#af-interface eth0/0` # This command will enter the sub-configuration for a specific EIGRP enabled interface

`R1(config-router-af-interface)#summary-address 172.16.0.0/22` # This command will allow you to send a summarized route update for all the prefixes that fall within the range.

# Full Configuration Below

![](/images/EIGRP%20summary%20address.png)

# Verification

### Now lets run our show ip route command again and see what it looks like. 

![](/images/show%20ip%20route%20eigrp%20after%20summarization.png)

### Now we can only see the /22 route that is being advertised to us. Lets take a look at the EIGRP update message via wireshark.

![](/images/wireshark%20after%20eigrp%20summarization.png)

# Conclusion

### After going through these steps we have successfully accomplished implementing some security on our EIGRP enabled links.

