# Standard ACLs

### In this guide we will cover configuring and applying a standard ACL to an interface and our VTY lines. Not shown in this guide is the configuration of static routes between routers, and the configuration of SSH. Refer to our other guides for those configuration steps.

![](/ACLs/standard_acl_network_image.png)

# Configuration 

### Security standards we need to configure

    1. Users in VLAN 10 should not be able to ping both servers
    2. Users in VLAN 20 should not be able to SSH to R2
    3. The file server should not be able to reach the ENG VLAN 20

### First lets figure where to apply the ACL. Standard ACLs should be as close to the destination as possible.

![](/ACLs/standard_acl_placement.png)

# CLI Configuration 

`R1(config)#access-list 10 [permit | deny] [any | host | Network] [Wildcard Mask]` # This command will configure a standard ACL and a Access Control Entry

`R1(config-if)#ip access-group [ Number | Name] [ in | out]` # This command will apply the ACL inbound or outbound on the desired interface

`R1(config-line)#ip access-class [ Number | Name ] [ in | out]` # This command will apply the ACL inbound or outbound on the VTY lines of the Cisco network device

### Now lets conmfigure and apply the security standards listed above

### - Security standard 1

`R1(config)#ip access-list 10 deny 10.10.10.0 0.0.0.255 log` # This command will configure a standard ACL and a Access Control Entry and also log any matches to the ACL

`R1(config)#ip access-list 10 permit any log` # This command will permit any traffic that does not match our first entry. We need this because of the **"implicit"** deny at the end of every ACL

`R1(config)#inteface eth0/0` # This command brings us into the sub-interface configuration mode

`R1(config-if)#ip access-group 10 out` # This command applies the ACL outbound on interface eth0/0

### Full configuration below

![](/ACLs/standard_acl_10_config.png)

### - Security standard 2

`R1(config)#access-list 20 deny 10.10.20.0 0.0.0.255 log` # This command will configure a standard ACL and a Access Control Entry and also log any traffic matches to the ACL

`R1(config)#ip access-list 10 permit any log` # This command will permit any traffic that does not match our first entry. We need this because of the **"implicit"** deny at the end of every ACL

`R1(config)#line vty 0 4` # This command will bring you into the VTY sub-configuration mode

`R1(config-line)#ip access-class 20 in` This command will apply the ACL inbound to the VTY lines only

### Full configuration below

![](/ACLs/standard_acl_20_config.png)

### - Security Standard 3

`R2(config)#access-list 30 deny host 10.10.10.9 log` # This command will create the ACL and only filter based on a single source IP address

`R2(config)#ip access-list 10 permit any log` # This command will permit any traffic that does not match our first entry. We need this because of the **"implicit"** deny at the end of every ACL

`R2(config)#interface eth0/0.20` # This command will bring us into the sub-interface configuration mode

`R2(config-subif)#ip access-class 30 out` # This command applies the ACL outbound on interface eth0/0.20

### Full configuration below

![](/ACLs/standard_acl_30_config.png)

# Testing / Verification

### Now lets do some testing. For this testing we will remove the ACL, check connectivity and then apply the ACL and test again. In this guide we will just demonstrate security standard 1. 

![](/ACLs/test_image_01.png)

### - As we can see here our VLAN 10 Users can currently ping the servers. Let's apply the ACL and see if our ping test is successful.

![](/ACLs/test_image_02.png)

![](/ACLs/test_image_03.png)

![](/ACLs/test_image_04.png)

### - As we can see now the ACL worked and our ping test responded with an **"unreachable"** message. We can also see from our show command that the ACL has 5 matches on our first Access Control Entry.

`R1#show ip access-lists [ number | name]` # This command will show all access-lists or specific access-lists that we have created on our device

![](/ACLs/show_ip_access_list_cmd.png)

`R1#show access-lists [number | name]` # This command will show all access-lists or specific access-lists that we have created on our device

![](/ACLs/test_image_04.png)

`R1#show run | se access` # This command will show us in the running-configuration what access-lists configuration we currently have.

![](/ACLs/show_run_access_lists.png)

# Conclusion

### Standard ACLs are a quick and easy way to deny/filter traffic for a whole subnet or a single host. But what if we want our users in VLAN 10 to still reach the Servers, but not be able to SSH or use HTTP on the web server? Standard ACLs can not get the job done. Extended ACLs can give us more granular control over what we filter on our networks.