# Extended and Named ACLs

### In this guide we will configure a numbered extended ACL and a named extended ACL. Not shown in this guide is the configuration of static routes between routers, and the configuration of SSH. Refer to our other guides for those configuration steps.

![](/ACLs/extended_acl_network_diagarm.png)

# Configuration

### Security standards we need to configure
    1. Users from VLAN 10 and VLAN 20 should not be able to telnet or make http connections to the web server
    2. Only users from the ENG VLAN 20 should have SSH access to R2

### First lets figure where to apply the ACL. Standard ACLs should be as close to the destination as possible. 

![](/ACLs/extended_acls_placement.png)

# CLI Configuration

### Extended Numbered

`R1(config)#access-list 130 [ permit | deny ] [ protocol ] [ source IP or Protocol ] [ destination IP or Protocol] eq [ applicaiton | protocol]` # This syntax/command is just a simple overview of how to configure an Access Control Entry in an Extended ACL. There is a LOT more options you can configure on an extended ACL

![](/ACLs/extended_acl_config_example.png)

`R1(config-if)#ip access-group [ Number | Name] [ in | out]` # This command will apply the ACL inbound or outbound on the desired interface

`R1(config-line)#ip access-class [ Number | Name ] [ in | out]` # This command will apply the ACL inbound or outbound on the VTY lines of the Cisco network device

![](/ACLs/named_acl_example.png)

### Named Configuration

`R1(config)#ip access-list [ standard | extended] [ number | name]` # This command creates a named ACL. Named ACL's have administrative advantages. You can be specific with the name and purpose of ACL, and you edit/order the Access Control Entries easier with **"named"** ACLs.

`R1(config-ext-nacl)#[ permit | deny ] [ protocol ] [ source IP or Protocol ] [ destination IP or Protocol] eq [ applicaiton | protocol]` # This command/syntax is just a simple overiew of how to configure an Access Control Entry in a named extended ACL.

### Now lets conmfigure and apply the security standards listed above

### - Security standard 1 - Named ACL

`R1(config)#ip access-list extended DENY_HTTP_TELNET` # This command creates an extended named ACL

`R1(config-ext-nacl)#deny tcp 10.10.10.0 0.0.0.255 10.10.9.0 0.0.0.255 eq www`

`R1(config-ext-nacl)#deny tcp 10.10.20.0 0.0.0.255 10.10.9.0 0.0.0.255 eq www`

`R1(config-ext-nacl)#deny tcp 10.10.10.0 0.0.0.255 10.10.9.0 0.0.0.255 eq 23`

`R1(config-ext-nacl)#deny tcp 10.10.20.0 0.0.0.255 10.10.9.0 0.0.0.255 eq 23` # These Access Control entries deny telnet and http access from the SALES and ENG VLANs

`R1(config-ext-nacl)#permit tcp any any log` # This command allows all other traffic to the servers

`R1(config)#interface eth0/0.10` # This command brings us into the sub-interface configuration mode

`R1(config-if)#ip access-group DENY_HTTP_TELNET in` This command applies the ACL inbound under the sub-interface

### Full configuration is shown below

![](/ACLs/extended_acl_config_01.png)

### Security Standard 2 - numbered ACL

`R1(config)#access-list 130 permit tcp 10.10.20.0 0.0.0.255 any eq 22` # This command will create ACL 130 and the first Access Control Entry

`R1(config)#line vty 0 4` # This command will bring us into the line vty sub-configuration

`R1(config-line)#access-class 130 in` # This command will apply the ACL in bound on the VTY lines

### Full configuration is shown below

![](/ACLs/extended_acl_config_02.png)

# Testing / Verification

### Now lets do some testing. For this testing we will remove the ACL, check connectivity and then apply the ACL and test again. In this guide we will just demonstrate security standard 1.

![](/ACLs/test_telnet_without_acl.png)

![](/ACLs/test_telnet_without_acl.png)

### - As we can see telnet and HTTP connections are successful to our web server in the 10.10.9.0/24 subnet. Now lets apply our ACL inbound on sub-interface 0/0.10.

`R2(config-subif)ip access-group DENY_TELNET_HTTP in`

![](/ACLs/test_with_acl.png)

![](/ACLs/syslog_filter_message.png)

### - Just from the CLI we can see that our traffic is getting blocked. On R2 we also get a syslog message alerting us that one of one Access Control Entries has a match.

![](/ACLs/wireshark_telnet_receive.png)

![](/ACLs/wireshark_telnet_unavailable_message.png)

### - Looking at our wireshark capture we can see that our interface does recieve the telnet request, but replies with an ICMP unavailable messages. In the ICMP message we see the **"Communication administratively filtered"** message.

# Editing ACLs

### Editing Names ACLs is simple. We go into the ACL named sub-configuration mode, and then we can resequence our Access Control Entries, or delete an Access Control Entry.

![](/ACLs/show_extended_acl_log_matches.png)

`R1(config-ext-nacl)#no 10 deny tcp 10.10.10.0 0.0.0.255 10.10.9.0 0.0.0.255 eq telnet log` # This command deletes the Access Control Entry.

`R1(config-ext-nacl)#5 deny tcp 10.10.30.0 0.0.0.255 10.10.9.0 0.0.0.255 eq telnet log` # This command will place this Access Control Entry before all the other entries.

![](/ACLs/edit_named_acl.png)
![](/ACLs/show_extended_acl_after_edit.png)

### Editing numbered ACLs is not as simple as a named ACL. To edit a numbered ACL we must copy and paste the ACL to a text editor, make our changes there, delete the ACL, and then re-paste the ACL configuration. This will not be shown in this giude, but will be demonsrated in the corresponding videos.